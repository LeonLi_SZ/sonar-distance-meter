led.set_brightness(80)
basic.show_icon(IconNames.HEART)
basic.pause(1000)
basic.show_icon(IconNames.NO)
basic.pause(1000)

def on_forever():
    serial.write_line(convert_to_text(sonar.ping(DigitalPin.P0, DigitalPin.P1, PingUnit.CENTIMETERS)))
    led.toggle(2, 2)
    basic.pause(200)
basic.forever(on_forever)
